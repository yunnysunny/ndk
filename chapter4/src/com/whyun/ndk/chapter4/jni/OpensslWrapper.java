package com.whyun.ndk.chapter4.jni;

public class OpensslWrapper {
    public native void parseP12File(String filename,String password) throws Exception;
    static {
    	System.loadLibrary("chapter4");
    }
}
