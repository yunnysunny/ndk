LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := crypto
LOCAL_SRC_FILES := libcrypto.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE    := libchapter4

#LOCAL_C_INCLUDES := $(SYSROOT)/usr/include
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog
LOCAL_SRC_FILES := chapter4.c

LOCAL_SHARED_LIBRARIES := crypto
include $(BUILD_SHARED_LIBRARY)

TARGET_PLATFORM := android-3

$(info $(SYSROOT))
$(info $(LOCAL_C_INCLUDES))


